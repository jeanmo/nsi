Site destiné aux élèves de première générale en spé NSI au lycée Jean Monnet d'Annemasse (France, Haute-Savoie 74).

Son rendu est visible [sur https://jeanmo.forge.apps.education.fr/nsi/](https://jeanmo.forge.apps.education.fr/nsi/).

**Merci** au passage aux contributeurs et contributrices pour les nombreux outils et services développés et mis à disposition, pour toutes ces connaissances partagées !